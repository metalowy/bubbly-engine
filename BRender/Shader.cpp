#include "BRender.h"

namespace BubblyRender
{
	Shader::Shader()
	{
		program = glCreateProgram();
		uniforms = new std::map<std::string, GLint>();

		if (!program)
		{
			std::cerr << "[ShaderError] Shader creation failed" << std::endl;
			exit(1);
		}
	}
	//****************************************************************************************
	void Shader::addVertexShader(std::string text)
	{
		addProgram(text, GL_VERTEX_SHADER);
	}
	//----------------------------------------------------------------------------------------
	void Shader::addFragmentShader(std::string text)
	{
		addProgram(text, GL_FRAGMENT_SHADER);
	}
	//----------------------------------------------------------------------------------------
	void Shader::addGeometryShader(std::string text)
	{
		addProgram(text, GL_GEOMETRY_SHADER);
	}
	//****************************************************************************************
	void Shader::addUniform(std::string text)
	{
		const GLchar *source = (const GLchar *)text.c_str();
		GLint uniformLocation = glGetUniformLocation(program, source);

		if (uniformLocation == 0xFFFFFFFF)
		{
			std::cerr << "[ShaderError] Could not find uniform: " << text << std::endl;
			exit(1);
		}

		uniforms->insert({ text, uniformLocation });
	}
	//----------------------------------------------------------------------------------------
	void Shader::setUniformi(std::string text, GLint value)
	{
		glUniform1i(uniforms->at(text), value);
	}
	//----------------------------------------------------------------------------------------
	void Shader::setUniformf(std::string text, GLfloat value)
	{
		glUniform1f(uniforms->at(text), value);
	}
	//----------------------------------------------------------------------------------------
	void Shader::setUniform(std::string text, glm::vec3 value)
	{
		glUniform3f(uniforms->at(text), value.x, value.y, value.z);
	}
	//----------------------------------------------------------------------------------------
	void Shader::setUniform(std::string text, glm::mat4* value)
	{
		glUniformMatrix4fv(uniforms->at(text), 1, GL_FALSE, glm::value_ptr(*value));
	}
	//****************************************************************************************
	void Shader::compileShader()
	{
		glLinkProgram(program);

		CheckShaderError(program, GL_LINK_STATUS, true, "[ShaderError] Program failed to link: ");

		glValidateProgram(program);
		CheckShaderError(program, GL_VALIDATE_STATUS, true, "[ShaderError] Program failed to validate: ");
	}
	//----------------------------------------------------------------------------------------
	void Shader::bind()
	{
		glUseProgram(program);
	}
	//****************************************************************************************
	void Shader::addProgram(std::string text, int type)
	{
		int shader = glCreateShader(type);

		if (!shader)
		{
			std::cerr << "[ShaderError] Adding shader failed" << std::endl;
			exit(1);
		}

		const GLchar *source = (const GLchar *)text.c_str();
		glShaderSource(shader, 1, &source, 0);
		glCompileShader(shader);

		CheckShaderError(shader, GL_COMPILE_STATUS, false, "[ShaderError] Shader compilation failed: ");

		glAttachShader(program, shader);
	}
	//----------------------------------------------------------------------------------------
	void Shader::CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string errorMessage)
	{
		GLint success = 0;
		GLchar error[1024] = { 0 };

		if (isProgram) glGetProgramiv(shader, flag, &success);
		else glGetShaderiv(shader, flag, &success);

		if (success == GL_FALSE)
		{
			if (isProgram) glGetProgramInfoLog(shader, sizeof(error), NULL, error);
			else glGetShaderInfoLog(shader, sizeof(error), NULL, error);

			std::cerr << errorMessage << ": '" << error << "'" << std::endl;
			exit(1);
		}
	}
	//----------------------------------------------------------------------------------------
	Shader::~Shader()
	{
		delete this->uniforms;
	}
}