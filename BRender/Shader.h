#pragma once

#include "BRender.h"

namespace BubblyRender
{
	class BRENDERDLL_API Shader
	{
	private:
		GLint program;
		std::map<std::string, GLint>* uniforms;

	public:
		Shader();
		~Shader();
		void addVertexShader(std::string text);
		void addFragmentShader(std::string text);
		void addGeometryShader(std::string text);

		void addUniform(std::string text);
		void setUniformi(std::string text, GLint value);
		void setUniformf(std::string text, GLfloat value);
		void setUniform(std::string text, glm::vec3 value);
		void setUniform(std::string text, glm::mat4* value);

		void compileShader();
		void bind();	

	private:		
		void addProgram(std::string text, int type);
		static void CheckShaderError(GLuint shader, GLuint flag, bool isProgram, const std::string errorMessage);
	};

}