#pragma once

#include "BRender.h"

namespace BubblyRender
{
	class BRENDERDLL_API ResourceLoader
	{
	public:
		static std::string loadShader(std::string fileName);

	};
}