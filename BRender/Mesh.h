#pragma once

#include "BRender.h"

#define SIZE 3

namespace BubblyRender
{
	class BRENDERDLL_API Mesh
	{
	private:
		enum
		{
			POSITION_VB,

			NUM_BUFFERS
		};

		GLuint m_vertexArrayObject;
		GLuint m_vertexArrayBuffers[NUM_BUFFERS];
		int m_drawCount;

	public:
		Mesh();
		void addVertices(Vertex* vertices);
		void draw();

		~Mesh();
	};
}