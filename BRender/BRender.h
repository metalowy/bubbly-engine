#pragma once
#ifndef BRENDERDLL_EXPORTS
#define BRENDERDLL_API __declspec(dllexport) 
#else
#define BRENDERDLL_API __declspec(dllimport) 
#endif

#define DIAGNOSTICS

#include <GL\glew.h>
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <SDL2\SDL.h>

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

#include <map>

#include "Vertex.h"
#include "Mesh.h"
#include "ResourceLoader.h"
#include "Shader.h"
#include "Transform.h"



inline float ToRad(float angle)
{
	return angle * (static_cast<float>(3.1415) / static_cast<float>(180));
}