#pragma once

#include "BRender.h"

namespace BubblyRender
{
	class BRENDERDLL_API Transform
	{
	private:
		glm::vec3* translation;
		glm::vec3* rotation;
		glm::vec3* scale;

	public:
		Transform();
		~Transform();

		glm::vec3 getTranslation();
		void setTranslation(glm::vec3 translation);
		void setTranslation(float x, float y, float z);

		glm::vec3 getRotation();
		void setRotation(glm::vec3 rotation);
		void setRotation(float x, float y, float z);

		glm::vec3 getScale();
		void setScale(glm::vec3 scale);
		void setScale(float x, float y, float z);

		glm::mat4 getTransformation();

	};
}