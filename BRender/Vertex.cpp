#include "BRender.h"

namespace BubblyRender
{
	Vertex::Vertex(glm::vec3 pos) : pos(pos) {};
	Vertex::~Vertex(){};
	
	void Vertex::setPos(glm::vec3 pos){ this->pos = pos; };

	glm::vec3 Vertex::getPos() { return this->pos; }
}