#include "BRender.h"

namespace BubblyRender
{
	Transform::Transform()
	{
		translation = new glm::vec3(0.0f, 0.0f, 0.0f);
		rotation = new glm::vec3(0.0f, 0.0f, 0.0f);
		scale = new glm::vec3(1.0f, 1.0f, 1.0f);
	}

	Transform::~Transform()
	{
		delete this->translation;
		delete this->rotation;
		delete this->scale;
	}
	//------------------------------------------------------------------
	glm::vec3 Transform::getTranslation()
	{
		return *this->translation;
	}

	void Transform::setTranslation(glm::vec3 translation)
	{
		this->translation->x = translation.x;
		this->translation->y = translation.y;
		this->translation->z = translation.z;
	}

	void Transform::setTranslation(float x, float y, float z)
	{
		this->translation->x = x;
		this->translation->y = y;
		this->translation->z = z;
	}
	//------------------------------------------------------------------
	glm::vec3 Transform::getRotation()
	{
		return *this->rotation;
	}

	void Transform::setRotation(glm::vec3 rotation)
	{
		this->rotation->x = rotation.x;
		this->rotation->y = rotation.y;
		this->rotation->z = rotation.z;
	}

	void Transform::setRotation(float x, float y, float z)
	{
		this->rotation->x = x;
		this->rotation->y = y;
		this->rotation->z = z;
	}
	//------------------------------------------------------------------
	glm::vec3 Transform::getScale()
	{
		return *this->scale;
	}

	void Transform::setScale(glm::vec3 scale)
	{
		this->scale->x = scale.x;
		this->scale->y = scale.y;
		this->scale->z = scale.z;
	}

	void Transform::setScale(float x, float y, float z)
	{
		this->scale->x = x;
		this->scale->y = y;
		this->scale->z = z;
	}
	//------------------------------------------------------------------
	glm::mat4 Transform::getTransformation()
	{
		glm::mat4 transformation = glm::mat4(1.0);
		glm::mat4 rotation_x = glm::mat4(1.0);
		glm::mat4 rotation_y = glm::mat4(1.0);
		glm::mat4 rotation_z = glm::mat4(1.0);
		glm::mat4 scale_m = glm::mat4(1.0);

		float rad_x = ToRad(this->rotation->x);
		float rad_y = ToRad(this->rotation->y);
		float rad_z = ToRad(this->rotation->z);

		transformation[3][0] = this->translation->x;
		transformation[3][1] = this->translation->y;
		transformation[3][2] = this->translation->z;

		rotation_x[1][1] = cos(rad_x);
		rotation_x[1][2] = -sin(rad_x);
		rotation_x[2][1] = sin(rad_x);
		rotation_x[2][2] = cos(rad_x);

		rotation_y[0][0] = cos(rad_y);
		rotation_y[0][2] = -sin(rad_y);
		rotation_y[2][0] = sin(rad_y);
		rotation_y[1][2] = cos(rad_y);

		rotation_z[0][0] = cos(rad_z);
		rotation_z[1][0] = -sin(rad_z);
		rotation_z[0][1] = sin(rad_z);
		rotation_z[1][1] = cos(rad_z);

		scale_m[0][0] = this->scale->x;
		scale_m[1][1] = this->scale->y;
		scale_m[2][2] = this->scale->z;

		return scale_m * transformation * rotation_z * rotation_y * rotation_x;
	}

}