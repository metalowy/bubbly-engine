#include "BRender.h"


namespace BubblyRender
{
	std::string ResourceLoader::loadShader(std::string fileName)
	{
		std::stringstream shaderSource;
		std::ifstream shaderFile;

		std::string t;

		shaderFile.open("../Debug/res/shaders/" + fileName, std::ios::in);

		if (shaderFile.is_open())
		{
			while (shaderFile.good())
			{
				getline(shaderFile, t);
				shaderSource << t << "\n";
			}
		}	
		else
		{
			std::cerr << "[LoaderError] Unable to open file: " << fileName << std::endl;
		}

		shaderFile.close();
		

		return shaderSource.str();
	}
}