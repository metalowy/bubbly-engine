#pragma once

#include "BRender.h"

#define SIZE 3

namespace BubblyRender
{
	class BRENDERDLL_API Vertex
	{
	private:
		glm::vec3 pos;

	public:
		Vertex(glm::vec3 pos);
		~Vertex();
		void setPos(glm::vec3 pos);
		glm::vec3 getPos();
	};
}