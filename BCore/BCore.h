#pragma once
#ifndef BCOREDLL_EXPORTS
#define BCOREDLL_API __declspec(dllexport) 
#else
#define BCOREDLL_API __declspec(dllimport) 
#endif

#define DIAGNOSTICS

#include <GL\glew.h>
#include <SDL2\SDL.h>
#include <SDL2\SDL_keyboard.h>
#include <SDL2\SDL_keycode.h>
#include <glm\glm.hpp>

#include "..\BRender\Mesh.h"
#include "..\BRender\Vertex.h"

#include "BubblyDisplay.h"
#include "BubblyGame.h"
#include "MainBubble.h"
#include "BubblyTime.h"
#include "BubblyInput.h"