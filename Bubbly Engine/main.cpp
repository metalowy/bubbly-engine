// Built-in
#include <iostream>

// DLLs
#include "..\BCore\BCore.h"
#include "..\BRender\BRender.h"

// classes


// OpenGL


// util
namespace BC = BubblyCore;
namespace BR = BubblyRender;

// Main entry
int main(int argc, char** argv)
{
	// Welcome toast
	std::cout << "Welcome to Bubbly Engine (bEngine)!" << std::endl;
	
	// Main game object
	std::string gameTitle = "BubblyGame";
	BC::MainBubble game = BC::MainBubble(gameTitle);

	game.Start();

	return 0;
}