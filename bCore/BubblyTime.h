#pragma once

#include "BCore.h"

#include <chrono>

typedef std::chrono::time_point<std::chrono::system_clock, std::chrono::system_clock::duration> BChronoTime;

namespace BubblyCore
{
	class BubblyTime
	{
		friend class BubblyCore::MainBubble;
	public:
		enum
		{
			FRAME_CAP = 5000,
			SECOND = 1000000000,
		};

	private:
		static BChronoTime bStartTime;
		static double delta;
		BCOREDLL_API static void setDelta(double sDelta);

	public:
		BCOREDLL_API static double getDelta();
		BCOREDLL_API static long getTime();		
		
	};


}