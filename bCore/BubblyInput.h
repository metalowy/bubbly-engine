#pragma once

#include "BCore.h"

#define NUM_KEYS 512

namespace BubblyCore
{
	class BubblyInput
	{
	private:
		static Uint8 currentKeys[NUM_KEYS];
		static Uint8 downKeys[NUM_KEYS];
		static Uint8 upKeys[NUM_KEYS];
		static Uint32 mouseState;
		static Uint32 mouseDown;
		static Uint32 mouseUp;
		static glm::vec2 mousePos;
	public:
		static BCOREDLL_API void update();
		// Keyboard
		static BCOREDLL_API bool getKey(int keyCode);
		static BCOREDLL_API bool getKeyDown(int keyCode);
		static BCOREDLL_API bool getKeyUp(int keyCode);
		// Mouse
		static BCOREDLL_API glm::vec2 getMousePosition();
		static BCOREDLL_API bool getMouse(int mouse);
		static BCOREDLL_API bool getMouseDown(int mouse);
		static BCOREDLL_API bool getMouseUp(int mouse);
	};


}