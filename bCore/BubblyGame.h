#pragma once

#include "BCore.h"

namespace BubblyCore
{
	class BCOREDLL_API BubblyGame
	{
	private: 
		BubblyRender::Mesh mesh;
		BubblyRender::Shader shader;
		BubblyRender::Transform transform;
	
	public:
		BubblyGame();
		~BubblyGame();

		void Input();
		void Update();
		void Render();
	};
}