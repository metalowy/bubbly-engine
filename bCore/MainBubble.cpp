#include "BCore.h"

namespace BubblyCore
{
	// Constructor
	MainBubble::MainBubble(std::string& title)
	{
		this->pbDisplay = new BubblyDisplay(640, 480, title);

		this->pbGame = new BubblyGame();

		isRunning = false;
	}

	// Start method for main object
	void MainBubble::Start()
	{
		if (isRunning)
		{
			return;
		}

#ifdef DIAGNOSTICS
		//Diagnostics-----------------------------------------------//
		time_t currentTime;											//
		struct tm *localTime;										//
		time(&currentTime);											//
		localTime = localtime(&currentTime);						//
																	//
		std::cout << localTime->tm_hour << ":"						//
			<< localTime->tm_min << ":"								//
			<< localTime->tm_sec << " | MainBubble <"				//
			<< pbDisplay->GetTitle() << "> started running\n"		//
			<< std::endl;											//
		//----------------------------------------------------------//
#endif

		Run();
	}

	// Stop method
	void MainBubble::Stop()
	{
		if (!isRunning)
			return;

		isRunning = false;
	}

	// Run method
	void MainBubble::Run()
	{
		isRunning = true;

		int frames = 0;
		long frameCounter = 0;
		const double frameTime = 1.0 / BubblyTime::FRAME_CAP;
		BubblyTime::bStartTime = std::chrono::high_resolution_clock::now();
		double unprocessedTime = 0;

		while (isRunning)
		{
			bool render = false;
			long lastDelta = BubblyTime::getTime();
			unprocessedTime += lastDelta / static_cast<double>(BubblyTime::SECOND);
			frameCounter += lastDelta;
			
			while (unprocessedTime > frameTime)
			{
				render = true;
				unprocessedTime -= frameTime;

				///------------------------
				BubblyTime::setDelta(frameTime);							
				///------------------------
				// Update Game
				pbGame->Update();
				pbGame->Input();
				//-------------------------
				// FrameCounter
				if (frameCounter >= BubblyTime::SECOND)
				{
#ifdef DIAGNOSTICS
					std::cout << "FPS: " << frames << "   \r";
#endif
					frames = 0;
					frameCounter = 0;
				}

				if (pbDisplay->IsClosed())
					Stop();
			}

			if (render)
			{
				frames++;
				Render();
			}
			else
			{
				_sleep(1);
			}

		}

		CleanUp();
	}

	// Render method
	void MainBubble::Render()
	{
		BubblyDisplay::ClearScreen();
		pbGame->Render();
		pbDisplay->Update();
	}

	// Clean up method
	void MainBubble::CleanUp()
	{
		// It is C++ so it has destructor to deal with that kind of stuff
		// May be usefull later
	}

	MainBubble::~MainBubble()
	{
		delete this->pbGame;
		delete this->pbDisplay;
	};
}