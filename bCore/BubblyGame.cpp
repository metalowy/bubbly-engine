#include "BCore.h"

namespace BubblyCore
{
	BubblyGame::BubblyGame()
	{
		BubblyRender::Vertex vertices[] = {
			BubblyRender::Vertex(glm::vec3(-1, -1, 0)),
			BubblyRender::Vertex(glm::vec3(0, 1, 0)),
			BubblyRender::Vertex(glm::vec3(1, -1, 0))
		};

		shader.addVertexShader(BubblyRender::ResourceLoader::loadShader("basicVertex.vertex"));
		shader.addFragmentShader(BubblyRender::ResourceLoader::loadShader("basicFragment.fragment"));
		shader.compileShader();

		shader.addUniform("transform");

		mesh.addVertices(vertices);
	}

	void BubblyGame::Input()
	{
		BubblyInput::update();
	}

	void BubblyGame::Update()
	{
		static float t = 0.0f;
		
		t += BubblyTime::getDelta();

		float scale = 0.1f;

		transform.setTranslation((float)sin(t), 0, 0);
		transform.setRotation(0, sin(t) * 20, sin(t) * 180);
		transform.setScale(scale, scale, scale);
	}

	void BubblyGame::Render()
	{
		
		shader.bind();
		shader.setUniform("transform", &transform.getTransformation());
		mesh.draw();
	}

	BubblyGame::~BubblyGame(){};
}