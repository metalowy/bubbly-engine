#pragma once

#include "BCore.h"

namespace BubblyCore
{
	class BCOREDLL_API MainBubble
	{
	public:

	private:
		BubblyGame* pbGame;
		BubblyCore::BubblyDisplay* pbDisplay;
		bool isRunning;

	public:
		MainBubble(std::string& title);
		~MainBubble();
		void Start();
		void Stop();
	private:
		void Run();
		void Render();
		void CleanUp();
	};

}