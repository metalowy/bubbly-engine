#include "BCore.h"

namespace BubblyCore
{
	BubblyDisplay::BubblyDisplay(int width, int height, std::string& title)
	{
		//tricky - for windows type application only
		SDL_Init(SDL_INIT_EVERYTHING);
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		//tricky end

		b_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL);
		b_glContext = SDL_GL_CreateContext(b_window);


		std::cout << "Running on OpenGL version: \n" << (char*)(glGetString(GL_VERSION)) << std::endl << std::endl;

		if (glewInit() != GLEW_OK)
		{
			std::cerr << "Failed to initialize GLEW" << std::endl;
		}

		b_isClosed = false;

		//OpenGL options init
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glFrontFace(GL_CW);
		glCullFace(GL_BACK);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		//TODO depth clamp

		glEnable(GL_FRAMEBUFFER_SRGB);
		
	}


	BubblyDisplay::~BubblyDisplay()
	{
		SDL_GL_DeleteContext(b_glContext);
		SDL_DestroyWindow(b_window);
		SDL_Quit();
	}

	void BubblyDisplay::ClearScreen()
	{
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void BubblyDisplay::Update()
	{
		SDL_GL_SwapWindow(b_window);

		SDL_Event e;

		while (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT) b_isClosed = true;
		}
	}

	bool BubblyDisplay::IsClosed()
	{
		return b_isClosed;
	}

	int BubblyDisplay::GetWidth()
	{
		int w;
		SDL_GetWindowSize(b_window, &w, nullptr);
		return w;
	}

	int BubblyDisplay::GetHeight()
	{
		int h;
		SDL_GetWindowSize(b_window, nullptr, &h);
		return h;
	}

	std::string BubblyDisplay::GetTitle()
	{
		return std::string(SDL_GetWindowTitle(b_window));
	}

}