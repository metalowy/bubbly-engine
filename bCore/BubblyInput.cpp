#include "BCore.h"

namespace BubblyCore
{
	Uint8 BubblyInput::currentKeys[NUM_KEYS] = { 0 };
	Uint8 BubblyInput::downKeys[NUM_KEYS] = { 0 };
	Uint8 BubblyInput::upKeys[NUM_KEYS] = { 0 };
	Uint32 BubblyInput::mouseState = 0;
	Uint32 BubblyInput::mouseDown = 0;
	Uint32 BubblyInput::mouseUp = 0;
	glm::vec2 BubblyInput::mousePos = glm::vec2();

	void BubblyInput::update()
	{
		// Keyboard
		const Uint8 *upKeysT = SDL_GetKeyboardState(NULL);

		for (int i = 0; i < NUM_KEYS; i++)
		{
			if (currentKeys[i] && !upKeysT[i])
			{
				upKeys[i] = 1;
			}
			else
			{
				upKeys[i] = 0;
			}
		}

		const Uint8 *downKeysT = SDL_GetKeyboardState(NULL);
			
		for (int i = 0; i < NUM_KEYS; i++)
		{
			if (!downKeysT[i])
			{
				downKeys[i] = 0;
			}
			if (downKeysT[i] && currentKeys[i])
			{
				downKeys[i] = 0;
			}
			if (downKeysT[i] && !currentKeys[i])
			{
				downKeys[i] = 1;
			}				
		}

		const Uint8 *currentKeysT = SDL_GetKeyboardState(NULL);

		for (int i = 0; i < NUM_KEYS; i++)
		{
			currentKeys[i] = currentKeysT[i];
		}


		// Mouse
		int x;
		int y;
		Uint32 mouse = SDL_GetMouseState(&x, &y);
		mousePos = glm::vec2(x, y);
		
		if (mouseState == mouse)
		{
			mouseDown = 0;
		}
		else
		{
			mouseDown = mouse;
		}

		if (mouseState != mouse)
		{
			mouseUp = mouseState;
		}
		else
		{
			mouseUp = 0;
		}

		mouseState = mouse;

	}
	// Keyboard
	bool BubblyInput::getKey(int keyCode)
	{
		return static_cast<bool>(currentKeys[keyCode]);
	}

	bool BubblyInput::getKeyDown(int keyCode)
	{
		return static_cast<bool>(downKeys[keyCode]);
	}

	bool BubblyInput::getKeyUp(int keyCode)
	{
		return static_cast<bool>(upKeys[keyCode]);
	}

	// Mouse
	glm::vec2 BubblyInput::getMousePosition()
	{
		return mousePos;
	}

	bool BubblyInput::getMouse(int mouse)
	{
		return mouseState == mouse;
	}

	bool BubblyInput::getMouseDown(int mouse)
	{
		return mouseDown == mouse;
	}

	bool BubblyInput::getMouseUp(int mouse)
	{
		return mouseUp == mouse;
	}
}