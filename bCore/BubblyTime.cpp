#include "BCore.h"

namespace BubblyCore
{
	BChronoTime BubblyTime::bStartTime = BChronoTime();
	double BubblyTime::delta = 0.0;

	long BubblyTime::getTime()
	{
		BChronoTime previous = bStartTime;
		BChronoTime bCurrentTime = bStartTime = std::chrono::high_resolution_clock::now();
		return static_cast<long>(std::chrono::duration_cast<std::chrono::nanoseconds>(bCurrentTime - previous).count());
	}

	double BubblyTime::getDelta()
	{
		return BubblyTime::delta;
	}

	void BubblyTime::setDelta(double sDelta)
	{
		BubblyTime::delta = sDelta;
	}

}