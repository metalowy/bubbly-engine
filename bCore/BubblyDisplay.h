#pragma once

#include "BCore.h"

#include <string>
#include <iostream>

namespace BubblyCore
{
	class BubblyDisplay
	{
	private:
		SDL_Window* b_window;
		SDL_GLContext b_glContext;
	public:
		//construction shit
		BCOREDLL_API BubblyDisplay(int width, int height, std::string& title);
		BCOREDLL_API virtual ~BubblyDisplay();
		//construction shit end

		BCOREDLL_API static void ClearScreen();
		BCOREDLL_API void Update();
		BCOREDLL_API bool IsClosed();
		BCOREDLL_API int GetWidth();
		BCOREDLL_API int GetHeight();
		BCOREDLL_API std::string GetTitle();

	private:
		bool b_isClosed;

	};
}